# Raspberry Pi 4 based Access Point

This raspberry pi image contains the RaspAP - Webgui application. See https://raspap.com/ for details. The raspberry pi os lite is updated March 3, 2021.

## Installation

Use balena-etcher or any sd card (4GB and above) image writer app for your pc to write the raspbian.zip image to your micro-sd card. Boot the raspberry pi 4 using the written micro-sd card.

## Usage
Wait for about 5min and you should be able to detect a network called raspi-webgui using a client pc. Connect to that network using the following credentials 
```bash
SSID: raspi-webgui
Password: ChangeMe
```
You may connect to your device's hostapd via a web-gui by connecting to 
```bash
IP address: 10.3.141.1
    Username: admin
    Password: secret
```
or using ssh.


## License
[MIT](https://choosealicense.com/licenses/mit/)
